<?php

return [
    'statsd' => [
        /**
            * The host of your DogStatsD server. If this is not set the Agent looks at the DD_AGENT_HOST environment
            * variable.
            */
        'host' => env('DATADOG_AGENT_HOST', '127.0.0.1'),

        /**
            * The port of your DogStatsD server. If this is not set, the Agent looks at the DD_DOGSTATSD_PORT
            * environment variable.
            */
        'port' => env('DATADOG_DOGSTATSD_PORT', '8125'),

        /**
            * The path to the DogStatsD Unix domain socket (overrides host and port). This is only supported with Agent
            * v6+.
            */
        'socket_path' => env('DATADOG_DOGSTATSD_SOCKET_PATH'),
    ],

    'api' => [
        'host' => 'https://app.datadoghq.eu',

        'api_key' => env('DATADOG_API_KEY'),

        'application_key' => env('DATADOG_APPLICATION_KEY'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Datadog Tracking Prefix
    |--------------------------------------------------------------------------
    |
    | This is the prefix that will be placed in front of all of your metric entries. If you have multiple
    | applications being tracked in Datadog, it is recommended putting the application name somewhere
    | inside of your prefix. A common naming scheme is something like app.<app-name>.
    |
    */
    'metric_prefix' => env('APP_NAME').'.'.env('APP_ENV', 'local'),

    /**
        * Tags to apply to all metrics, events, and service checks. The @dd.internal.entity_id tag is appended to
        * global_tags from the DD_ENTITY_ID environment variable.
        */
    'global_tags' => [
        'app' => env('APP_NAME'),
        'env' => env('APP_ENV'),
    ],
];
