<?php

namespace DatadogLaravel\DatadogLaravel;

use DatadogLaravel\DatadogLaravel\Support\Events\Event;
use DatadogLaravel\DatadogLaravel\Support\Metrics\Metric;
use DatadogLaravel\DatadogLaravel\Support\Service\FakeDatadogClient;
use DatadogLaravel\DatadogLaravel\Support\Service\IDatadogClient;
use DatadogLaravel\DatadogLaravel\Support\ServiceChecks\ServiceCheck;
use Illuminate\Support\Facades\Facade;

class Datadog extends Facade
{
    /**
     * Replace the bound instance with a fake.
     *
     * @return FakeDatadogClient
     */
    public static function fake(): FakeDatadogClient
    {
        static::swap($fake = new FakeDatadogClient());

        return $fake;
    }

    public static function makeMetric(string $name): Metric
    {
        /** @var IDatadogClient $datadogClient */
        $datadogClient = static::getFacadeRoot();

        return new Metric($datadogClient, $name);
    }

    public static function makeServiceCheck(string $name): ServiceCheck
    {
        /** @var IDatadogClient $datadogClient */
        $datadogClient = static::getFacadeRoot();

        return new ServiceCheck($datadogClient, $name);
    }

    public static function makeEvent(string $title, string $text = null): Event
    {
        /** @var IDatadogClient $datadogClient */
        $datadogClient = static::getFacadeRoot();

        return new Event($datadogClient, $title, $text);
    }

    /**
     * @inheritdoc
     * @return class-string<IDatadogClient>
     */
    public static function getFacadeAccessor(): string
    {
        return IDatadogClient::class;
    }
}
