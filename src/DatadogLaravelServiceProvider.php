<?php

namespace DatadogLaravel\DatadogLaravel;

use DataDog\DogStatsd;
use DatadogLaravel\DatadogLaravel\Events\CustomEventsToDatadog;
use DatadogLaravel\DatadogLaravel\Metrics\DbQueryExecutionTimeMetric;
use DatadogLaravel\DatadogLaravel\Metrics\ResponseTimeMetric;
use DatadogLaravel\DatadogLaravel\Support\Service\DatadogClient;
use DatadogLaravel\DatadogLaravel\Support\Service\FakeDatadogClient;
use DatadogLaravel\DatadogLaravel\Support\Service\IDatadogClient;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class DatadogLaravelServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(DogStatsd::class, function (Application $app) {
            /** @var Repository $config */
            $config = $app->make('config');

            $dataDogConfig = [
                'host' => $config->get('datadog.statsd.host'),
                'port' => $config->get('datadog.statsd.port'),
                'socket_path' => $config->get('datadog.statsd.socket_path'),
                'datadog_host' => $config->get('datadog.api.host'),
                'api_key' => $config->get('datadog.api.api_key'),
                'app_key' => $config->get('datadog.api.application_key'),
                'global_tags' => $config->get('datadog.global_tags'),
                'metric_prefix' => $config->get('datadog.metric_prefix'),
            ];

            return new DogStatsd($dataDogConfig);
        });

        $this->app->singleton(IDatadogClient::class, function (Application $app) {
            if ($app->runningUnitTests()) {
                return new FakeDatadogClient();
            }

            /** @var DogStatsd $dogStatsd */
            $dogStatsd = $app->make(DogStatsd::class);

            return new DatadogClient($dogStatsd);
        });
    }

    /**
     * @return void
     */
    public function boot(): void
    {
        $this->publishes([
            __DIR__.'/../config/datadog.php' => config_path('datadog.php'),
        ]);

        if (App::isProduction()) {
            resolve(ResponseTimeMetric::class)->register();
            resolve(DbQueryExecutionTimeMetric::class)->register();
            resolve(CustomEventsToDatadog::class)->register();
        }
    }
}
