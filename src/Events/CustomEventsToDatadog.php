<?php

namespace DatadogLaravel\DatadogLaravel\Events;

use DatadogLaravel\DatadogLaravel\Support\Events\DispatchDatadogEvent;
use DatadogLaravel\DatadogLaravel\Support\Metrics\Registerable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;

class CustomEventsToDatadog implements Registerable
{
    /**
     * @return void
     */
    public function register(): void
    {
        Event::listen('*', function (string $eventName, array $eventData) {
            $event = Arr::get($eventData, 0);

            if (!$event instanceof DispatchDatadogEvent) {
                return;
            }

            $event->toDatadogEvent()->report();
        });
    }
}
