<?php

namespace DatadogLaravel\DatadogLaravel\Testing;

class Event
{
    /**
     * @param  string $title
     * @param  array  $vals
     */
    public function __construct(
        private readonly string $title,
        private readonly array $vals = []
    ) {
    }

    /**
     * @param  string  $title
     * @param  array|null  $vals
     *
     * @return bool
     */
    public function matches(
        string $title,
        ?array $vals = []
    ): bool {
        return $this->title === $title
            && $this->vals === $vals;
    }
}
