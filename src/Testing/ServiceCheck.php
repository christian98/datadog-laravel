<?php

namespace DatadogLaravel\DatadogLaravel\Testing;

use DatadogLaravel\DatadogLaravel\Support\ServiceChecks\ServiceCheckStatus;

class ServiceCheck
{
    public function __construct(
        private readonly string $name,
        private readonly ServiceCheckStatus $status,
        private readonly null|string $message,
        private readonly null|string|array $tags
    ) {
    }

    public function matches(
        string $name,
        ServiceCheckStatus $status,
        ?string $message,
        null|string|array $tags,
    ): bool {
        if ($this->name !== $name || $this->message !== $message || $this->status !== $status) {
            return false;
        }

        if ((!is_null($tags) || !is_null($this->tags)) && $tags !== $this->tags) {
            return false;
        }

        return true;
    }
}
