<?php

namespace DatadogLaravel\DatadogLaravel\Testing;

enum MetricType
{
    case Gauge;
    case Histogram;
    case Distribution;
    case Count;
    case Increment;
    case Decrement;
    case Set;
    case MicroTiming;
    case Timing;
}
