<?php

namespace DatadogLaravel\DatadogLaravel\Testing;

class Metric
{
    /**
     * @param  array|string  $name
     * @param  MetricType  $type
     * @param  string|float  $value
     * @param  float|null  $sampleRate
     * @param  array|null  $tags
     */
    public function __construct(
        private readonly array|string $name,
        private readonly MetricType $type,
        private readonly string|float $value,
        private readonly ?float $sampleRate,
        private readonly string|null|array $tags,
    ) {
    }

    public function matches(
        array|string $name,
        MetricType $type,
        null|string|float $value,
        ?float $sampleRate,
        null|string|array $tags,
    ): bool {
        $shouldMatchValue = !is_null($value);
        if ($this->name !== $name || ($shouldMatchValue && $this->value !== $value) || $this->type !== $type) {
            return false;
        }

        if ((!is_null($sampleRate) || !is_null($this->sampleRate)) && $sampleRate !== $this->sampleRate) {
            return false;
        }

        if ((!is_null($tags) || !is_null($this->tags)) && $tags !== $this->tags) {
            return false;
        }

        return true;
    }
}
