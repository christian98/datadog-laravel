<?php

namespace DatadogLaravel\DatadogLaravel\Support\Events;

enum AlertType: string
{
    case Error = 'error';
    case Warning = 'warning';
    case Success = 'success';
    case Info = 'info';
}
