<?php

namespace DatadogLaravel\DatadogLaravel\Support\Events;

enum EventPriority: string
{
    case Low = 'low';
    case Normal = 'normal';
}
