<?php

namespace DatadogLaravel\DatadogLaravel\Support\Events;

use DatadogLaravel\DatadogLaravel\Support\Report;
use DatadogLaravel\DatadogLaravel\Support\Service\IDatadogClient;
use JetBrains\PhpStorm\ArrayShape;

class Event extends Report
{
    private readonly string $title;

    private readonly ?string $text;

    private ?string $aggregationKey = null;

    private ?EventPriority $priority = null;

    private ?AlertType $alertType = null;

    private readonly IDatadogClient $datadog;

    public function __construct(IDatadogClient $datadog, string $title, ?string $text)
    {
        $this->datadog = $datadog;
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * report this event to DataDog.
     *
     * @return bool|null
     */
    public function report(): ?bool
    {
        return $this->datadog->event($this->title, $this->payload());
    }

    /**
     * @return array
     */
    #[ArrayShape([
        'text' => "string", 'aggregation_key' => "null|string", 'priority' => "null|string",
        'source_type_name' => "null", 'alert_type' => "null|string", 'tags' => "mixed",
    ])]
    private function payload(): array
    {
        return [
            'text' => $this->text,
            // Hide as it will be set to current time by DogStatsD server
            // 'timestamp' => null,
            // TODO Hidden as this should never change? Maybe set to APP_NAME or APP_URL??
            // 'hostname' => null,
            'aggregation_key' => $this->aggregationKey,
            'priority' => $this->priority?->value,
            'source_type_name' => null,
            'alert_type' => $this->alertType?->value,
            'tags' => $this->tags,
        ];
    }

    /**
     * @param  string  $aggregationKey
     *
     * @return static
     */
    public function withAggregationKey(string $aggregationKey): static
    {
        $this->aggregationKey = $aggregationKey;

        return $this;
    }

    /**
     * @param  EventPriority  $priority
     *
     * @return static
     */
    protected function withPriority(EventPriority $priority): static
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @param  AlertType  $alertType
     *
     * @return static
     */
    public function withAlertType(AlertType $alertType): static
    {
        $this->alertType = $alertType;

        return $this;
    }

    public function asError(): static
    {
        return $this->withAlertType(AlertType::Error);
    }

    public function asInfo(): static
    {
        return $this->withAlertType(AlertType::Info);
    }

    public function asSuccess(): static
    {
        return $this->withAlertType(AlertType::Success);
    }

    public function asWarning(): static
    {
        return $this->withAlertType(AlertType::Warning);
    }
}
