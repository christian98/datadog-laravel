<?php

namespace DatadogLaravel\DatadogLaravel\Support\Events;

interface DispatchDatadogEvent
{
    public function toDatadogEvent(): Event;
}
