<?php

namespace DatadogLaravel\DatadogLaravel\Support\Metrics;

use DatadogLaravel\DatadogLaravel\Support\Report;
use DatadogLaravel\DatadogLaravel\Support\Service\IDatadogClient;

class Metric extends Report
{
    private readonly string $name;

    private float $sampleRate = 1.0;

    private readonly IDatadogClient $datadog;

    private ?DatadogTimer $timer = null;

    public function __construct(IDatadogClient $datadog, string $name)
    {
        $this->name = $name;
        $this->datadog = $datadog;
    }

    /**
     * @param  float $sampleRate
     *
     * @return static
     */
    public function withSampleRate(float $sampleRate): self
    {
        $this->sampleRate = $sampleRate;

        return $this;
    }

    /**
     * @return DatadogTimer
     */
    public function timer(): DatadogTimer
    {
        if (is_null($this->timer)) {
            $this->timer = new DatadogTimer($this);
        }

        return $this->timer;
    }

    /**
     * Updates one or more stats counters by arbitrary amounts.
     *
     * @param  int $delta The amount to increment/decrement each metric by.
     * @return void
     **/
    public function reportCount(int $delta): void
    {
        $this->datadog->count($this->name, $delta, $this->sampleRate, $this->tags);
    }

    /**
     * Increments one or more stats counters
     *
     * @param  int $increment  the amount to increment by (default 1)
     *
     * @return void
     **/
    public function reportIncrement(int $increment = 1): void
    {
        $this->datadog->increment($this->name, $increment, $this->sampleRate, $this->tags);
    }

    /**
     * Decrements one or more stats counters.
     *
     * @param  int $decrement the amount to decrement by (default -1)
     * @return void
     **/
    public function reportDecrement(int $decrement = 1): void
    {
        $this->datadog->decrement($this->name, $decrement, $this->sampleRate, $this->tags);
    }

    /**
     * Gauge
     *
     * @param  float $value The value
     * @return void
     **/
    public function reportGauge(float $value): void
    {
        $this->datadog->gauge($this->name, $value, $this->sampleRate, $this->tags);
    }

    /**
     * Log timing information
     *
     * @param  float $time The elapsed time (ms) to log
     * @return void
     */
    public function reportTiming(float $time): void
    {
        $this->datadog->timing($this->name, $time, $this->sampleRate, $this->tags);
    }

    /**
     * A convenient alias for the timing function when used with micro-timing
     *
     * @param  float $time The elapsed time to log, IN SECONDS
     * @return void
     **/
    public function reportMicroTiming(float $time): void
    {
        $this->datadog->microTiming($this->name, $time, $this->sampleRate, $this->tags);
    }

    /**
     * Distribution
     *
     * @param  float $value The value
     * @return void
     **/
    public function reportDistribution(float $value): void
    {
        $this->datadog->distribution($this->name, $value, $this->sampleRate, $this->tags);
    }

    /**
     * Set
     *
     * @param  float|string  $value The value
     *
     * @return void
     **/
    public function reportSet(float|string $value): void
    {
        $this->datadog->set($this->name, $value, $this->sampleRate, $this->tags);
    }

    /**
     * Histogram
     *
     * @param  float $value The value
     * @return void
     **/
    public function reportHistogram(float $value): void
    {
        $this->datadog->histogram($this->name, $value, $this->sampleRate, $this->tags);
    }
}
