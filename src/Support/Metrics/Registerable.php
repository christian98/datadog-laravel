<?php

namespace DatadogLaravel\DatadogLaravel\Support\Metrics;

interface Registerable
{
    public function register(): void;
}
