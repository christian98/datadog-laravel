<?php

namespace DatadogLaravel\DatadogLaravel\Support\Metrics;

use RuntimeException;

class DatadogTimer
{
    protected ?float $startTime = null;

    protected ?float $stopTime = null;

    /**
     * @param  Metric  $metric
     */
    public function __construct(
        protected Metric $metric
    ) {
    }

    /**
     * @return void
     */
    public function start(): void
    {
        $this->startTime = microtime(true);
    }

    /**
     * @return void
     */
    public function stopAndReport(): void
    {
        $this->stop();
        $this->report();
    }

    /**
     * @return void
     */
    public function stop(): void
    {
        if (is_null($this->startTime)) {
            throw new RuntimeException('You need to start the Timer before you can stop it');
        }

        $this->stopTime = microtime(true);
    }

    /**
     * @return void
     */
    public function report(): void
    {
        if (is_null($this->startTime) || is_null($this->stopTime)) {
            throw new RuntimeException('You need to measure the time first using start() and stop() or stopAndReport()');
        }

        $this->metric->reportMicroTiming($this->stopTime - $this->startTime);
    }

    /**
     * @return void
     */
    public function reset(): void
    {
        $this->startTime = null;
        $this->stopTime = null;
    }
}
