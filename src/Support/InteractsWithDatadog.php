<?php

namespace DatadogLaravel\DatadogLaravel\Support;

use DatadogLaravel\DatadogLaravel\Support\Service\IDatadogClient;
use function resolve;

trait InteractsWithDatadog
{
    /**
     * Get the Datadog client
     *
     * @return IDatadogClient
     */
    protected function datadog(): IDatadogClient
    {
        return resolve(IDatadogClient::class);
    }
}
