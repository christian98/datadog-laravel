<?php

namespace DatadogLaravel\DatadogLaravel\Support\Service;

interface IDatadogClient
{
    /**
     * Distribution
     *
     * @param  string            $stat       The metric
     * @param  float             $value      The value
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     *
     * @return void
     **/
    public function distribution(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void;

    /**
     * Histogram
     *
     * @param  string            $stat       The metric
     * @param  float             $value      The value
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     *
     * @return void
     **/
    public function histogram(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void;

    /**
     * Send a custom service check status over UDP
     *
     * @param  string            $name      service check name
     * @param  int               $status    service check status code (see OK, WARNING,...)
     * @param  array|string|null $tags      Key Value array of Tag => Value, or single tag as string
     * @param  string|null       $hostname  hostname to associate with this service check status
     * @param  string|null       $message   message to associate with this service check status
     * @param  int|null          $timestamp timestamp for the service check status (defaults to now)
     *
     * @return void
     **/
    public function serviceCheck(
        string $name,
        int $status,
        array|string $tags = null,
        string $hostname = null,
        string $message = null,
        int $timestamp = null
    ): void;

    /**
     * Decrements one or more stats counters.
     *
     * @param  array|string      $stats      The metric(s) to decrement.
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     * @param  int               $value      the amount to decrement by (default -1)
     *
     * @return void
     **/
    public function decrement(
        array|string $stats,
        int $value = -1,
        float $sampleRate = 1.0,
        array|string $tags = null
    ): void;

    /**
     * Increments one or more stats counters
     *
     * @param  array|string      $stats      The metric(s) to increment.
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     * @param  int               $value      the amount to increment by (default 1)
     *
     * @return void
     **/
    public function increment(
        array|string $stats,
        int $value = 1,
        float $sampleRate = 1.0,
        array|string $tags = null
    ): void;

    /**
     * A convenient alias for the timing function when used with micro-timing
     *
     * @param  string            $stat       The metric name
     * @param  float             $time       The elapsed time to log, IN SECONDS
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     *
     * @return void
     **/
    public function microTiming(string $stat, float $time, float $sampleRate = 1.0, array|string $tags = null): void;

    /**
     * Set
     *
     * @param  string            $stat       The metric
     * @param  float|string      $value      The value
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     *
     * @return void
     **/
    public function set(string $stat, float|string $value, float $sampleRate = 1.0, array|string $tags = null): void;

    /**
     * Log timing information
     *
     * @param  string            $stat       The metric to in log timing info for.
     * @param  float             $time       The elapsed time (ms) to log
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     *
     * @return void
     */
    public function timing(string $stat, float $time, float $sampleRate = 1.0, array|string $tags = null): void;

    /**
     * Updates one or more stats counters by arbitrary amounts.
     *
     * @param  array|string      $stats      The metric(s) to update. Should be either a string or array of metrics.
     * @param  int               $delta      The amount to increment/decrement each metric by.
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     *
     * @return void
     **/
    public function count(
        array|string $stats,
        int $delta = 1,
        float $sampleRate = 1.0,
        array|string $tags = null
    ): void;

    /**
     * Send an event to the Datadog HTTP api. Potentially slow, so avoid
     * making many call in a row if you don't want to stall your app.
     * Requires PHP >= 5.3.0
     *
     * @param  string $title Title of the event
     * @param  array  $vals  Optional values of the event. See
     *                       https://docs.datadoghq.com/api/?lang=bash#post-an-event
     *                       for the valid keys
     *
     * @return bool|null
     */
    public function event(string $title, array $vals = []): ?bool;

    /**
     * Gauge
     *
     * @param  string            $stat       The metric
     * @param  float             $value      The value
     * @param  float             $sampleRate the rate (0-1) for sampling.
     * @param  array|string|null $tags       Key Value array of Tag => Value, or single tag as string
     *
     * @return void
     **/
    public function gauge(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void;
}
