<?php

namespace DatadogLaravel\DatadogLaravel\Support\Service;

use DatadogLaravel\DatadogLaravel\Support\ServiceChecks\ServiceCheckStatus;
use DatadogLaravel\DatadogLaravel\Testing\Event;
use DatadogLaravel\DatadogLaravel\Testing\Metric;
use DatadogLaravel\DatadogLaravel\Testing\MetricType;
use DatadogLaravel\DatadogLaravel\Testing\ServiceCheck;
use Illuminate\Support\Arr;
use PHPUnit\Framework\Assert as PHPUnit;

class FakeDatadogClient implements IDatadogClient
{
    /**
     * @var array<int, Metric>
     */
    private array $reportedMetrics = [];

    /**
     * @var array<int, ServiceCheck>
     */
    private array $reportedServiceChecks = [];

    /**
     * @var array<int, Event>
     */
    private array $reportedEvents = [];

    public function assertReportedDistribution(
        string $stat,
        ?float $value,
        ?float $sampleRate,
        null|array|string $tags
    ): void {
        PHPUnit::assertTrue(
            $this->reportedMetric($stat, MetricType::Distribution, $value, $sampleRate, $tags),
            "The metric {$stat} was not reported"
        );
    }

    public function reportedMetric(
        string $stat,
        MetricType $type,
        ?float $value,
        ?float $sampleRate,
        null|array|string $tags
    ): bool {
        $matchedReportedMetrics = Arr::where(
            $this->reportedMetrics,
            function (Metric $metric) use ($type, $value, $tags, $sampleRate, $stat) {
                return $metric->matches($stat, $type, $value, $sampleRate, $tags);
            }
        );

        return count($matchedReportedMetrics) > 0;
    }

    public function distribution(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->reportedMetrics[] = new Metric($stat, MetricType::Distribution, $value, $sampleRate, $tags);
    }

    public function histogram(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->reportedMetrics[] = new Metric($stat, MetricType::Histogram, $value, $sampleRate, $tags);
    }

    public function serviceCheck(
        string $name,
        int $status,
        array|string $tags = null,
        string $hostname = null,
        string $message = null,
        int $timestamp = null
    ): void {
        $this->reportedServiceChecks[] = new ServiceCheck($name, ServiceCheckStatus::from($status), $message, $tags);
    }

    public function assertServiceCheckReported(
        string $name,
        ServiceCheckStatus $status,
        array|string $tags = null,
        string $hostname = null,
        string $message = null,
        int $timestamp = null,
    ): void {
        $matchedReportedServiceChecks = Arr::where(
            $this->reportedServiceChecks,
            function (ServiceCheck $serviceCheck) use ($message, $name, $status, $tags) {
                return $serviceCheck->matches($name, $status, $message, $tags);
            }
        );

        PHPUnit::assertTrue(count($matchedReportedServiceChecks) > 0, "The service check {$name} was not reported");
    }

    public function decrement(
        array|string $stats,
        int $value = -1,
        float $sampleRate = 1.0,
        array|string $tags = null
    ): void {
        $this->reportedMetrics[] = new Metric($stats, MetricType::Decrement, $value, $sampleRate, $tags);
    }

    public function increment(
        array|string $stats,
        int $value = 1,
        float $sampleRate = 1.0,
        array|string $tags = null
    ): void {
        $this->reportedMetrics[] = new Metric($stats, MetricType::Increment, $value, $sampleRate, $tags);
    }

    public function microTiming(string $stat, float $time, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->reportedMetrics[] = new Metric($stat, MetricType::MicroTiming, $time, $sampleRate, $tags);
    }

    public function set(string $stat, float|string $value, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->reportedMetrics[] = new Metric($stat, MetricType::Set, $value, $sampleRate, $tags);
    }

    public function timing(string $stat, float $time, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->reportedMetrics[] = new Metric($stat, MetricType::Timing, $time, $sampleRate, $tags);
    }

    public function count(array|string $stats, int $delta = 1, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->reportedMetrics[] = new Metric($stats, MetricType::Count, $delta, $sampleRate, $tags);
    }

    public function event(string $title, array $vals = []): ?bool
    {
        $this->reportedEvents[] = new Event($title, $vals);

        return true;
    }

    public function assertEventReported(string $title, array $vals = []): void
    {
        $matchedReportedEvents = Arr::where($this->reportedEvents, function (Event $event) use ($vals, $title) {
            return $event->matches($title, $vals);
        });

        PHPUnit::assertTrue(count($matchedReportedEvents) > 0, "The event {$title} was not reported");
    }

    public function gauge(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->reportedMetrics[] = new Metric($stat, MetricType::Gauge, $value, $sampleRate, $tags);
    }
}
