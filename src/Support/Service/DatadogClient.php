<?php

namespace DatadogLaravel\DatadogLaravel\Support\Service;

use DataDog\DogStatsd;
use Illuminate\Support\Traits\ForwardsCalls;

/**
 * @mixin DogStatsd
 */
class DatadogClient implements IDatadogClient
{
    use ForwardsCalls;

    /**
     * @param  DogStatsd  $client
     */
    public function __construct(private DogStatsd $client)
    {
    }

    public function timing(string $stat, float $time, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->client->timing($stat, $time, $sampleRate, $tags);
    }

    public function microTiming(string $stat, float $time, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->client->microtiming($stat, $time, $sampleRate, $tags);
    }

    public function gauge(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->client->gauge($stat, $value, $sampleRate, $tags);
    }

    public function histogram(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->client->histogram($stat, $value, $sampleRate, $tags);
    }

    public function distribution(string $stat, float $value, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->client->distribution($stat, $value, $sampleRate, $tags);
    }

    public function set(string $stat, float|string $value, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->client->set($stat, $value, $sampleRate, $tags);
    }

    public function increment(
        array|string $stats,
        int $value = 1,
        float $sampleRate = 1.0,
        array|string $tags = null
    ): void {
        $this->client->increment($stats, $sampleRate, $tags, $value);
    }

    public function decrement(
        array|string $stats,
        int $value = -1,
        float $sampleRate = 1.0,
        array|string $tags = null
    ): void {
        $this->client->decrement($stats, $sampleRate, $tags, $value);
    }

    public function count(array|string $stats, int $delta = 1, float $sampleRate = 1.0, array|string $tags = null): void
    {
        $this->client->updateStats($stats, $delta, $sampleRate, $tags);
    }

    public function serviceCheck(
        string $name,
        int $status,
        array|string $tags = null,
        string $hostname = null,
        string $message = null,
        int $timestamp = null
    ): void {
        $this->client->serviceCheck($name, $status, $tags, $hostname, $message, $timestamp);
    }

    public function event(string $title, array $vals = []): ?bool
    {
        return $this->client->event($title, $vals);
    }

    /**
     * @param  string  $name
     * @param  array  $args
     *
     * @return mixed
     */
    public function __call(string $name, array $args): mixed
    {
        return $this->forwardCallTo($this->client, $name, $args);
    }
}
