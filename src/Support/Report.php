<?php

namespace DatadogLaravel\DatadogLaravel\Support;

use Illuminate\Support\Arr;

abstract class Report
{
    protected array $tags = [];

    /**
     * @param  string  $key
     * @param  string  $value
     *
     * @return static
     */
    public function withTag(string $key, string $value): static
    {
        Arr::set($this->tags, $key, $value);

        return $this;
    }

    /**
     * @param  array<string, string> $tags
     *
     * @return static
     */
    public function withTags(array $tags): static
    {
        $this->tags = array_merge($this->tags, $tags);

        return $this;
    }
}
