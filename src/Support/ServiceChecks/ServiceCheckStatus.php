<?php

namespace DatadogLaravel\DatadogLaravel\Support\ServiceChecks;

enum ServiceCheckStatus: int
{
    case Ok = 0;
    case Warn = 1;
    case Critical = 2;
    case Unknown = 3;
}
