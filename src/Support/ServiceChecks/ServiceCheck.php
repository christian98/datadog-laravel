<?php

namespace DatadogLaravel\DatadogLaravel\Support\ServiceChecks;

use DatadogLaravel\DatadogLaravel\Support\Report;
use DatadogLaravel\DatadogLaravel\Support\Service\IDatadogClient;

class ServiceCheck extends Report
{
    private readonly string $name;

    private ?string $message = null;

    private readonly IDatadogClient $datadog;

    public function __construct(IDatadogClient $datadog, string $name)
    {
        $this->name = $name;
        $this->datadog = $datadog;
    }

    /**
     * @param  string $message Additional information or a description of why the status occurred.
     *
     * @return static
     */
    public function withMessage(string $message): static
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param  ServiceCheckStatus $status A constant describing the service status
     *
     * @return void
     */
    public function report(ServiceCheckStatus $status): void
    {
        $this->datadog->serviceCheck(
            name: $this->name,
            status: $status->value,
            tags: $this->tags,
            message: $this->message
        );
    }

    /**
     * @return void
     */
    public function reportOk(): void
    {
        $this->report(ServiceCheckStatus::Ok);
    }

    /**
     * @return void
     */
    public function reportWarn(): void
    {
        $this->report(ServiceCheckStatus::Warn);
    }

    /**
     * @return void
     */
    public function reportCritical(): void
    {
        $this->report(ServiceCheckStatus::Critical);
    }

    /**
     * @return void
     */
    public function reportUnknown(): void
    {
        $this->report(ServiceCheckStatus::Unknown);
    }
}
