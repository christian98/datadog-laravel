<?php

namespace DatadogLaravel\DatadogLaravel\Metrics;

use DatadogLaravel\DatadogLaravel\Datadog;
use DatadogLaravel\DatadogLaravel\Support\Metrics\Registerable;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;

class DbQueryExecutionTimeMetric implements Registerable
{
    /**
     * @return void
     */
    public function register(): void
    {
        Log::debug('Registering DbQueryExecutionTimeMetric');

        Event::listen(QueryExecuted::class, function (QueryExecuted $event): void {
            Log::debug("The database query took {$event->time} milliseconds.");

            Datadog::makeMetric('db.query')
                ->withTag('status', 'executed')
                ->reportTiming($event->time);
        });
    }
}
