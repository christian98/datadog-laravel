<?php

namespace DatadogLaravel\DatadogLaravel\Metrics;

use DatadogLaravel\DatadogLaravel\Datadog;
use DatadogLaravel\DatadogLaravel\Support\Metrics\Registerable;
use function defined;
use Illuminate\Foundation\Http\Events\RequestHandled;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use RuntimeException;

class ResponseTimeMetric implements Registerable
{
    /**
     * @return void
     */
    public function register(): void
    {
        Log::debug('Registering ResponseTimeMetric');
        if (!defined('LARAVEL_START')) {
            throw new RuntimeException("LARAVEL_START constant isn't defined. Please define it in index.php");
        }
        $laravelStart = (float) LARAVEL_START;

        Event::listen(RequestHandled::class, function (RequestHandled $requestHandled) use ($laravelStart): void {
            $tags = [
                'code' => (string) $requestHandled->response->getStatusCode(),
                'method' => $requestHandled->request->method(),
            ];

            $responseTime = microtime(true) - $laravelStart;

            Log::debug("The request took {$responseTime} seconds.");

            Datadog::makeMetric('response_time')
                ->withTags($tags)
                ->reportMicroTiming($responseTime);
        });
    }
}
