[![pipeline status](https://gitlab.com/christian98/datadog-laravel/badges/main/pipeline.svg)](https://gitlab.com/christian98/datadog-laravel/-/commits/main)
[![coverage report](https://gitlab.com/christian98/datadog-laravel/badges/main/coverage.svg)](https://gitlab.com/christian98/datadog-laravel/-/commits/main)
[![Latest Release](https://gitlab.com/christian98/datadog-laravel/-/badges/release.svg)](https://gitlab.com/christian98/datadog-laravel/-/releases)
[![Packagist Downloads](https://img.shields.io/packagist/dm/christian98/datadog-laravel?logo=packagist)](https://packagist.org/packages/christian98/datadog-laravel)
[![Packagist License](https://img.shields.io/packagist/l/christian98/datadog-laravel)](https://packagist.org/packages/christian98/datadog-laravel)
[![Packagist Version](https://img.shields.io/packagist/v/christian98/datadog-laravel?logo=packagist&logoColor=white)](https://packagist.org/packages/christian98/datadog-laravel)
[![Packagist PHP Version Support](https://img.shields.io/packagist/php-v/christian98/datadog-laravel?logo=php&logoColor=white)](https://packagist.org/packages/christian98/datadog-laravel)
[![Maintenance](https://img.shields.io/maintenance/yes/2023)](https://gitlab.com/christian98/datadog-laravel)
[![Treeware (Trees)](https://img.shields.io/treeware/trees/christian98/datadog-laravel)](https://plant.treeware.earth/christian98/datadog-laravel)

# Datadog Laravel


## Licence
This package is [Treeware](https://treeware.earth). If you use it in production, then we ask that you [**buy the world a tree**](https://plant.treeware.earth/christian98/datadog-laravel) to thank us for our work. By contributing to the Treeware forest you’ll be creating employment for local families and restoring wildlife habitats.
