variables:
  GIT_STRATEGY: clone

stages:
  - installation
  - testing
  - security
  - release
  - deploy

workflow:
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH == "alpha" || $CI_COMMIT_BRANCH == "beta" || $CI_COMMIT_BRANCH == "main"

################
# INSTALLATION #
################
composer:
  stage: installation
  image: lorisleiva/laravel-docker:$PHP_VERSION
  before_script:
    - install-php-extensions sockets
    - echo $COMPOSER_FLAGS
  script:
    - php -v
    - composer --version
    - composer install --no-ansi --no-interaction --no-plugins --no-progress --no-scripts --no-suggest --optimize-autoloader
    - composer require "illuminate/support:$LARAVEL_VERSION" --no-update --no-interaction
    - composer update
    - composer dump-autoload
  artifacts:
    paths:
      - vendor/
      - composer.lock
    expire_in: 1 days
    when: always
  except:
    - tags
  parallel:
    matrix:
      - PHP_VERSION: ["8.2", "8.3"]
        LARAVEL_VERSION: ["^10"]

###########
# TESTING #
###########
codestyle:
  stage: testing
  image: lorisleiva/laravel-docker:8.3
  script:
    - composer cs-fix -- --dry-run
  needs:
    - composer
  rules:
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_ID
  interruptible: true

phpunit:
  stage: testing
  image: lorisleiva/laravel-docker:$PHP_VERSION
  before_script:
    - !reference [composer, before_script]
    - !reference [composer, script]
    #    - composer require illuminate/support:$LARAVEL_VERSION
    - ./vendor/phpunit/phpunit/phpunit --version
    # Set the composer process timeout to the max pipeline run time
    - export COMPOSER_PROCESS_TIMEOUT=3600
  script:
    - composer test:ci
  coverage: /^\s*Lines:\s*(\d+.\d+\%)/
  artifacts:
    reports:
      junit: junit.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.cobertura.xml
    paths:
      - ./storage/logs # for debugging
    expire_in: 5 days
    when: always
  needs: []
  rules:
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_ID
  parallel:
    matrix:
      - PHP_VERSION: ["8.2", "8.3"]
        LARAVEL_VERSION: ["^10"]

larastan:
  stage: testing
  image: lorisleiva/laravel-docker:$PHP_VERSION
  before_script:
    - !reference [ composer, before_script ]
    - !reference [ composer, script ]
    #    - composer require illuminate/support:$LARAVEL_VERSION
    # Set the composer process timeout to the max pipeline run time
    - export COMPOSER_PROCESS_TIMEOUT=3600
  script:
    - composer phpstan -- --no-progress --error-format gitlab | tee phpstan.json
  artifacts:
    when: always
    reports:
      codequality: phpstan.json
  needs: []
  rules:
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_ID
  interruptible: true
  parallel:
    matrix:
      - PHP_VERSION: [ "8.2", "8.3" ]
        LARAVEL_VERSION: ["^10"]

############
# SECURITY #
############
security:composer:
  stage: security
  image: lorisleiva/laravel-docker:8.3
  script:
    - php vendor/bin/security-checker security:check composer.lock
  # Needs composer install as the security check is part of enligthn
  needs:
    - composer
  rules:
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_ID
  # allow_failure: true
  interruptible: true

release:
  image: node:22.14.0-alpine
  stage: release
  variables:
    GITLAB_TOKEN: $GITLAB_TOKEN
    GIT_STRATEGY: clone
  before_script:
    - apk add git
  script:
    - npm ci && npx semantic-release
  needs:
    - job: security:composer
      artifacts: false
    - job: larastan
      artifacts: false
      optional: true
    - job: phpunit
      artifacts: false
      optional: true
  only:
    refs:
      - main
      - alpha
    variables:
      - $CI_COMMIT_MESSAGE !~ /skip release/

deploy:
  stage: deploy
  image: alpine:latest
  script:
    - apk add curl
    - 'curl --header "Job-Token: $CI_JOB_TOKEN" --data tag=$CI_COMMIT_TAG "${CI_API_V4_URL}/projects/$CI_PROJECT_ID/packages/composer"'
  only:
    - tags
