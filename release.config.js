/** @type {import("semantic-release").Options } */
module.exports = {
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/gitlab",
        "@iwavesmedia/semantic-release-composer"
    ],
    branches: [
        "+([0-9])?(.{+([0-9]),x}).x",
        "main",
        "next",
        "next-major",
        {
            name: "alpha",
            prerelease: true,
        },
    ],
};
